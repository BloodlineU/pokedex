//
//  PokemonViewController.swift
//  Pokedex
//
//  Created by Alejandro Ulloa on 6/4/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher
import RealmSwift

class PokemonViewController: UIViewController, UITableViewDataSource {

    
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var typesTableView: UITableView!
    
    var pokemonName: String?
    var pokemonUrl: String = ""
    var types = [PokemonType]()
    var pokemonId: Int!
    var pokemon: Pokemon!
    var isFavourite: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = pokemonName?.capitalized ?? "Pokemon"
        downloadPokemonInfo()
        
        let button = UIBarButtonItem(title: isFavourite ? "Remove" : "Add", style: .done, target: self, action: #selector(favTapped))
        self.navigationItem.rightBarButtonItem = button
        
    }
    
    @objc func favTapped() {
        let userDefaults = UserDefaults.standard
        var favouritePokemon = userDefaults.array(forKey: "favPokemon") ?? []
        favouritePokemon.append(pokemonId)
        userDefaults.set(favouritePokemon, forKey: "favPokemon")
        
        let realm = try! Realm()
        try! realm.write {
            let pokemonEntity = PokemonEntity(pokemon: pokemon!)
            pokemonEntity.isFavourite = isFavourite ? false : true
            pokemonEntity.url = pokemonUrl
            realm.add(pokemonEntity, update: true)
        }
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    func downloadPokemonInfo() {
        Alamofire.request(pokemonUrl).responseObject { (response: DataResponse<Pokemon>) in
            self.pokemon = response.value
            
            self.heightLabel.text = "\(response.value?.height ?? 0)"
            self.weightLabel.text = "\(response.value?.weight ?? 0)"
            
            self.types = response.value?.types ?? []
            self.typesTableView.reloadData()
            
            self.pokemonId = response.value?.pokemonId
            
            self.pokemonImageView.kf.setImage(with: URL(string: response.value?.imageURL ?? "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/0.png"))
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = types[indexPath.row].name?.capitalized ?? "N/A"
        return cell
    }

}
