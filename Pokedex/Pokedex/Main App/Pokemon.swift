//
//  Pokemon.swift
//  Pokedex
//
//  Created by Alejandro Ulloa on 6/4/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import Foundation
import ObjectMapper

class PokemonResponse: Mappable {
    var pokemon: [Pokemon]?
    var nextURL: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        pokemon <- map["results"]
        nextURL <- map["next"]
    }
}

class Pokemon: Mappable {
    
    var pokemonId: Int?
    var name: String?
    var height: Double?
    var weight: Double?
    var imageURL: String?
    var types: [PokemonType]?
    var url: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map){
        pokemonId <- map["id"]
        name <- map["name"]
        height <- map["height"]
        weight <- map["weight"]
        imageURL <- map["sprites.front_default"]
        types <- map["types"]
        url <- map["url"]
    }
}


class PokemonType: Mappable {
    var name: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        name <- map["type.name"]
    }
}
