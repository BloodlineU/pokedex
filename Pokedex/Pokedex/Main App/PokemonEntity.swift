//
//  PokemonEntity.swift
//  Pokedex
//
//  Created by Alejandro Ulloa on 6/18/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import Foundation
import RealmSwift

class PokemonEntity: Object {

    @objc dynamic var pokemonId: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var height: Double = 0
    @objc dynamic var weight: Double = 0
    @objc dynamic var imageURL: String = ""
    @objc dynamic var isFavourite = false
    @objc dynamic var url: String = ""

    convenience init(pokemon: Pokemon){
        self.init()
        pokemonId = pokemon.pokemonId!
        name = pokemon.name!
        height = pokemon.height ?? 0
        weight = pokemon.weight ?? 0
        imageURL = pokemon.imageURL ?? ""
        url = pokemon.url ?? ""
        
    }
    
    override static func primaryKey() -> String {
        return "pokemonId"
    }
}
