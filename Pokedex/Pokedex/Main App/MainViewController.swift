//
//  MainViewController.swift
//  Pokedex
//
//  Created by Alejandro Ulloa on 5/10/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import RealmSwift

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate {
    
    @IBOutlet weak var pokedexTableView: UITableView!
    
    var nextURL = "https://pokeapi.co/api/v2/pokemon"
    var pokemon = [Pokemon]()
    
    var searchController: UISearchController!
    var filteredPokemon = [Pokemon]()
    var isFiltering = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPokemon()
        configSearchBar()
        
        
        
    }
    
    func getPokemon(){
        Alamofire
            .request(URL(string: nextURL)!)
            .responseObject{ (response: DataResponse <PokemonResponse>) in
                self.pokemon += response.value?.pokemon ?? []
                self.pokedexTableView.reloadData()
                self.nextURL = response.value?.nextURL ?? "https://pokeapi.co/api/v2/pokemon"
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering ? filteredPokemon.count : pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
        
        var currentPokemon: Pokemon!
        
        if isFiltering {
            currentPokemon = filteredPokemon[indexPath.row]
        } else {
            currentPokemon = pokemon[indexPath.row]
        }
        
        cell.pokemonNameLabel.text = currentPokemon.name?.capitalized ?? "n/a"
        
        let pokemonId = (pokemon.firstIndex { $0.name == currentPokemon.name } ?? 0) + 1
        
        cell.pokemonImageView.kf.setImage(with: URL(string: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(pokemonId).png" ))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toPokemonSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            if indexPath.row == pokemon.count - 1 {
                  getPokemon()
        }
    }
    
    //MARK :- SEARCH BAR
    
    func configSearchBar() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        
        pokedexTableView.tableHeaderView = searchController.searchBar
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredPokemon = pokemon.filter {
            ($0.name ?? "")
                .lowercased()
                .contains((searchController.searchBar.text ?? "")
                    .lowercased())
        }
        isFiltering = searchController.searchBar.text != ""
        pokedexTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPokemonSegue" {
            let destination = segue.destination as! PokemonViewController
            
            var selectedPokemon: Pokemon!
            
            if isFiltering {
                selectedPokemon = filteredPokemon[pokedexTableView.indexPathForSelectedRow?.row ?? 0]
            } else {
                selectedPokemon = pokemon[pokedexTableView.indexPathForSelectedRow?.row ?? 0]
            }
            
            searchController.dismiss(animated: true) {
                self.searchController.searchBar.text = ""
            }
            
            destination.pokemonUrl = selectedPokemon.url!
            destination.pokemonName = selectedPokemon.name
            destination.isFavourite = false
        }
    }
}

