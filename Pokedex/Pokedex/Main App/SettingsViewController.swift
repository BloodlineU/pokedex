//
//  SettingsViewController.swift
//  Pokedex
//
//  Created by Alejandro Ulloa on 6/14/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import FirebaseAuth

class SettingsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let button = UIBarButtonItem(title: "Sign out", style: .done, target: self, action: #selector(signOutTapped))
        self.navigationItem.rightBarButtonItem = button
    }
    
    @objc func signOutTapped() {
        do{
            try Auth.auth().signOut()
            dismiss(animated: true, completion: nil)
        } catch {
            
        }
    }

}
