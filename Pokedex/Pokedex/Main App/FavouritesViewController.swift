//
//  FavouritesViewController.swift
//  Pokedex
//
//  Created by Alejandro Ulloa on 6/18/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import RealmSwift

class FavouritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var favouritesTableView: UITableView!
    
    var pokemon = [PokemonEntity]()

    override func viewDidLoad() {
        super.viewDidLoad()

        getPokemon()
        self.favouritesTableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getPokemon()
        self.favouritesTableView.reloadData()
    }
    
    func getPokemon() {
        let realm = try! Realm()
        pokemon = Array(realm.objects(PokemonEntity.self).filter("isFavourite = true"))
        print(realm.configuration.fileURL)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(pokemon.count)
        return pokemon.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toPokemonSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
        cell.pokemonNameLabel.text = pokemon[indexPath.row].name.capitalized ?? "n/a"
        cell.pokemonImageView.kf.setImage(with: URL(string: pokemon[indexPath.row].imageURL ))
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPokemonSegue" {
            let destination = segue.destination as! PokemonViewController
            
            let selectedPokemon = pokemon[favouritesTableView.indexPathForSelectedRow?.row ?? 0]
            
            destination.pokemonUrl = selectedPokemon.url
            destination.pokemonName = selectedPokemon.name
            destination.isFavourite = true
        }
    }
}
