//
//  LoginViewController.swift
//  Pokedex
//
//  Created by Alejandro Ulloa on 4/16/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class LoginViewController: UIViewController {

    
    @IBOutlet weak var pokedexLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var pokedexImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //pokedexImageView.image = UIImage(named: "pokedex")
        //pokedexImageView.contentMode = .scaleAspectFit
        activityIndicator.hidesWhenStopped = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let db = Firestore.firestore()
        
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        
        let userRef = db.collection("user-info").document(currentUser.uid)
        
        userRef.getDocument { (snapshot,error) in
            self.activityIndicator.stopAnimating()
            if (error != nil) || snapshot?.get("registerCompleted") == nil {
                self.performSegue(withIdentifier: "toRegisterSegue", sender: self)
            } else {
                self.performSegue(withIdentifier: "toMainSegue", sender: self)
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }

    @IBAction func loginButtonPressed(_ sender: Any) {
        
        firebaseAuth(email: emailTextField.text!, password: passwordTextField.text!)
    }

    func firebaseAuth(email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) {(result,error) in
            if let _ = error {
                self.showAlertError()
                return
            }
            self.performSegue(withIdentifier: "toMainView", sender: self)
        }
    }
    
    func showAlertError(){
        let alertView = UIAlertController(title: "Error", message: "Please enter valid credentials", preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "Ok", style: .default) { [unowned self] (_) in
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
        }
        alertView.addAction(okAlertAction)
        present(alertView, animated: true, completion: nil)
    }
    

}
