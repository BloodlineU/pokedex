//
//  ViewController.swift
//  Pokedex
//
//  Created by Alejandro Ulloa on 4/12/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var helloLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBAction func changeButtonPressed(_ sender: Any, forEvent event: UIEvent) {
        helloLabel.text = "Hello Universe 2!"
    }
    
}

