//
//  CreateAccount.swift
//  Pokedex
//
//  Created by Alejandro Ulloa on 4/30/19.
//  Copyright © 2019 Alejandro Ulloa. All rights reserved.
//

import UIKit
import FirebaseAuth

class CreateAccount: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        confirmPasswordTextField.delegate = self

        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if passwordTextField.text! != confirmPasswordTextField.text {
            showAlertError(withMessage: "Passwords don't match!")
        }
    }
    
    @IBAction func createButtonPressed(_ sender: Any) {
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (result, error) in
            if let _ = error {
                self.showAlertError(withMessage: error?.localizedDescription ?? "Error")
            } else {
                let userInfoViewController = self.storyboard?.instantiateViewController(withIdentifier: "UserInfoViewController") as! UserInfoViewController
                self.present(userInfoViewController,animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func showAlertError(withMessage message:String){
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(okAlertAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
}
